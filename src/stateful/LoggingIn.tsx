import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux'

import { UsernameActions, setUsername } from '../handlers/store/username/actions'
import FormLogin from '../stateless/FormLogin/FormLogin'

export interface Props {
  setUsername(username: string): void
}
interface State {
  username: string
}

/* Gère input formulaire de login */

export class LoggingIn extends React.Component<Props, State>{
  constructor(props: Props){
    super(props)
    this.state = {
      username: ""
    }
    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  private onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ username: e.target.value })
  }

  private onSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    this.props.setUsername(this.state.username)
  }

  render(){
    return (
      <FormLogin 
        onChange={this.onChange} 
        onSubmit={this.onSubmit} 
        username={this.state.username}
      />
    )
  }
}


function mapDispatchToProps(dispatch: Dispatch<UsernameActions>) {
  return {
    setUsername: (username: string) => dispatch(setUsername(username)),
  }
}

export default connect(null, mapDispatchToProps)(LoggingIn);