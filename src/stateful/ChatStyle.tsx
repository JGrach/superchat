import * as React from 'react';

import Header from '../stateless/Header/Header'
import './ChatStyle.css'

export interface Props {
  children: React.ReactNode;
}

/* Fourni un style généric que je ne savais pas où mettre à cause du double emploi de ChatPage
Avec la disparition de chatPage, on prévoit la disparition de ChatStyle remplacé par un composant stateless
qui renverrais ChannelsHandlers et MessagesHandlers.*/

class ChatStyle extends React.Component<Props> {
  render(){
    return (
      <div id="chatStyle" className="default">
        <Header/>
        { React.Children.only(this.props.children) }
      </div>
    )
  }
}

export default ChatStyle;