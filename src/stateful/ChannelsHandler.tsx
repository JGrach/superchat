import * as React from 'react'
import { OpenChannel, GroupChannel, SendBirdError, SendBirdInstance } from 'sendbird';
import { StoreState } from 'src/handlers/store/store';
import { Dispatch } from 'redux';
import { ErrorActions, addError } from 'src/handlers/store/errors/actions';
import { ChannelActions, setCurrentChannel, addChannel, removeChannel } from 'src/handlers/store/channels/actions';
import { connect } from 'react-redux';
import * as ReactModal from 'react-modal'

import CreateChannel from './CreateChannel';
import ListChannel from '../stateless/ListChannel/ListChannel'

export interface Props {
  addSbError: (error: SendBirdError) => void
  addChannel: (channels: OpenChannel|GroupChannel) => void
  setCurrentChannel: (channel: OpenChannel|GroupChannel) => void
  removeChannel: (channel: OpenChannel|GroupChannel) => void
  sb: SendBirdInstance
  currentChannel: OpenChannel | GroupChannel
  channels: Array<OpenChannel | GroupChannel>
  username: string
}

interface State {
  showCreate: boolean
}

/* Gestion des channels du store - renvoie modal de creation de channel et listes de channels */

export class ChannelsHandler extends React.Component<Props, State> {
  constructor(props: Props){
    super(props)
    this.state = {
      showCreate: false
    }
    this.createGroupChannel = this.createGroupChannel.bind(this)
    this.changeChannel = this.changeChannel.bind(this)
    this.removeChannel = this.removeChannel.bind(this)
    this.showCreate = this.showCreate.bind(this)
  }

  private createGroupChannel(usersIds: string[], shouldFocus: boolean = true){
    if(this.state.showCreate) this.setState({showCreate: false})
    usersIds.push(this.props.username)
    const name = usersIds.join(', ')
    this.props.sb.GroupChannel.createChannelWithUserIds(usersIds, false, name, '', '', (groupChannel, error) => {
      if (error) this.props.addSbError(error)
      this.props.addChannel(groupChannel)
      if(shouldFocus) this.props.setCurrentChannel(groupChannel)
    });
  }

  private removeChannel(channel: OpenChannel|GroupChannel){
    if(channel == this.props.currentChannel) this.props.setCurrentChannel(this.props.channels[0])
    if('leave' in channel) channel.leave((error) => {if(error) this.props.addSbError})
    this.props.removeChannel(channel)
  }

  private changeChannel(channel: OpenChannel|GroupChannel){
    this.props.setCurrentChannel(channel)
  }

  private showCreate(e:React.MouseEvent<HTMLButtonElement>){
    this.setState({showCreate: !this.state.showCreate})
  }

  render(){
    const createValue = "create channel".toUpperCase() // maybe will join a dictionnary to language 
    return(
      <div id="channelsHandler" className="default">
        <ReactModal 
           isOpen={this.state.showCreate}
           contentLabel="createModal"
           overlayClassName="bgCreateModal"
           className="createModal"
        >
          <CreateChannel createChannel={this.createGroupChannel} closeModal={this.showCreate}/>
        </ReactModal>
        <button id="createModal" onClick={this.showCreate} >{createValue}</button>
        <ListChannel 
          currentChannel={this.props.currentChannel} 
          listChannels={this.props.channels} 
          changeChannel={this.changeChannel} 
          removeChannel={this.removeChannel}
        />
      </div>
    )
  }
}

function mapStateToProps({ channels, sendbird, username }: StoreState) {
  return {
    sb: sendbird.sb,
    channels: channels.channels,
    currentChannel: channels.currentChannel,
    username: username
  }
}

function mapDispatchToProps(dispatch: Dispatch<ErrorActions | ChannelActions>) {
  return {
    addError: (error: string): void => { dispatch(addError(error)) },
    addSbError: (error: SendBird.SendBirdError): void => { dispatch(addError("Sendbird error " + error.code + ": " + error.message)) },
    setCurrentChannel: (channel: OpenChannel|GroupChannel): void => { dispatch(setCurrentChannel(channel)) },
    addChannel: (channel: OpenChannel|GroupChannel): void => { dispatch(addChannel([channel], undefined)) },
    removeChannel: (channel: OpenChannel|GroupChannel): void => { dispatch(removeChannel([channel])) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChannelsHandler)