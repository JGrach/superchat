import * as React from 'react'
import { OpenChannel, GroupChannel, SendBirdError, UserMessage, FileMessage, AdminMessage } from 'sendbird';

import FormMessage from '../stateless/FormMessage/FormMessage'

export interface Props {
  channel: GroupChannel | OpenChannel
  addSbError: (error: SendBirdError) => void
  addMsg: (channel: OpenChannel | GroupChannel, message: UserMessage | FileMessage | AdminMessage) => void
}

interface State {
  message: string
  loading: boolean
}

/* Composant pour gérer les inputs d'entrée du formulaire de message */

export default class SendMessage extends React.Component<Props, State> {
  constructor(props: Props){
    super(props)
    this.state = {
      message: "",
      loading: false
    }
      this.onChange = this.onChange.bind(this)
      this.onSubmit = this.onSubmit.bind(this)
  }

  private onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ message: e.target.value })
  }

  private onSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    this.sendUserMessage(this.state.message)
    this.setState({ loading: true })
  }

  private sendUserMessage = (message: string): void => {
    this.props.channel.sendUserMessage(message, "", "", (message, error) => {
      if (error) { this.props.addSbError(error) }
      this.props.addMsg(this.props.channel, message as UserMessage)
      this.setState({ loading: false, message: "" })
    });
  }

  render(){
    return(
      <FormMessage message={ this.state.message } onChange={ this.onChange } onSubmit={ this.onSubmit }/>
    )
  }
}