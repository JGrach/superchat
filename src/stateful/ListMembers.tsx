import * as React from 'react'
import { OpenChannel, GroupChannel, SendBirdError, User } from 'sendbird';

export interface Props {
  channel: GroupChannel | OpenChannel
  addSbError: (error: SendBirdError) => void
}

interface State {
  members: Array<User>
}

/* Non implémenté, devrait changer de nom et devenir MembersHandlers pour rester cohérent avec le reste de l'app */

export class ListMembers extends React.Component<Props, State> {
  constructor(props: Props){
    super(props)
    this.state = {
      members: []
    }
    this.loadMembers(this.props.channel)
  }

  private loadMembers(channel: GroupChannel | OpenChannel): void {
    if(channel.isOpenChannel()) this.loadOpenMembers(channel as OpenChannel)
    else this.loadGroupMembers(channel as GroupChannel)
  }

  private loadGroupMembers(channel: GroupChannel){
    this.setState({
      members: channel.members
    })
  }

  private loadOpenMembers(channel: OpenChannel){
    var participantListQuery = channel.createParticipantListQuery();
    
    while(participantListQuery.hasNext){
      participantListQuery.next((participantList, error) => {
        if (error) { this.props.addSbError(error) }
        this.setState({
          members: [...this.state.members, ...participantList]
        })
      });
    }
  }

  render(){
    return(
      <div />
    )
  }
}