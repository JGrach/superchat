// 2 pages, just one initial condition to switch
// No need react router

import * as React from 'react';
import { connect } from 'react-redux';
import * as ReactModal from 'react-modal'

import { StoreState } from '../handlers/store/store'
import Login from '../stateless/Login/Login'
import ModalError from '../stateless/ModalError/ModalError'
import SendBirdMiddleware from './SendBirdMiddleware';
import ChatPage from './ChatPage'

export interface Props {
  username: string
  errors: string[]
}

interface State {
  username: string
  showError: boolean
}

/* On pourrait utiliser React Router mais pour deux pages ... */

export class Router extends React.Component<Props, State>{
  public state = {
    username: "",
    showError: false
  }

  public componentWillReceiveProps(newProps: Props){
    if(newProps.username !== this.state.username){
      this.setState({username: newProps.username})
    }
    if(newProps.errors.length > 0 !== this.state.showError){
      this.setState({showError: newProps.errors.length > 0})
    } 
  }

  public render(){
    return (
      <div className="default">
        {/* Modal de display d'erreur affiché quand this.state.showError true */}
        <ReactModal 
           isOpen={this.state.showError} 
           contentLabel="Error Modal"
           overlayClassName="bgErrorModal"
           className="errorModal"
        >
          <ModalError errors={ this.props.errors }/> 
        </ReactModal>
        {
          /* Router qui watch username setté par login et get par SendBirdMiddleware */
          !this.state.username ? 
          <Login/> : 
          <SendBirdMiddleware username={ this.state.username }>
            <ChatPage />
          </SendBirdMiddleware>
        }
      </div>
    )
  }
}

function mapStateToProps({ username, errors }: StoreState) {
  return {
    errors,
    username
  }
}

export default connect(mapStateToProps)(Router);