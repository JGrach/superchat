import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux'
import * as SendBird from 'sendbird';

import { ErrorActions, addError } from '../handlers/store/errors/actions';
import { SendbirdActions, setSendbird } from '../handlers/store/sendbird/actions';
import Loader from '../stateless/Loader/Loader' 

export interface Props {
  children: React.ReactNode;
  username: string;
  addError: (error: string) => void;
  addSbError: (error: SendBird.SendBirdError) => void;
  setSendbird: (sb: SendBird.SendBirdInstance, owner: SendBird.User) => void;
}

interface State {
  loaded: boolean
}

/* Devrait se renommer SendBirdInitialisation et quitter sa forme de provider pour redevenir un composant plus lambda */

class SendBirdMiddleware extends React.Component<Props, State> {
  constructor(props: Props){
    super(props)
    this.state = {
      loaded: false
    }
    this.loadSendBird()
  }

  private loadSendBird(){
    // create instance
    const sb = new SendBird({'appId': '10211D96-2ECE-4755-9740-97C1EEB00A6F'})

    // connect user
    sb.connect(this.props.username, (user, err) => {
      if(err) this.props.addSbError(err)

      // set preference
      sb.setChannelInvitationPreference(true, (res, err) => {
        if(err) this.props.addSbError(err)

        // export results
        this.props.setSendbird(sb, user)
        this.setState({
          loaded: true
        })
      })
    })
  }

  render(){
    return (
      <div id="sendbirdMiddleware" className="default">
        { this.state.loaded ? React.Children.only(this.props.children) : <Loader/>}
      </div>
    )
  }
}

function mapDispatchToProps(dispatch: Dispatch<ErrorActions | SendbirdActions>) {
  return {
    addError: (error: string): void => { dispatch(addError(error)) },
    addSbError: (error: SendBird.SendBirdError): void => { dispatch(addError("Sendbird error " + error.code + ": " + error.message)) },
    setSendbird: (sb: SendBird.SendBirdInstance, owner: SendBird.User): void => { dispatch(setSendbird(sb, owner)) }
  }
}

export default connect(null, mapDispatchToProps)(SendBirdMiddleware);