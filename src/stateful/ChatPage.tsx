import * as React from 'react'
import { OpenChannel, GroupChannel, SendBirdInstance, SendBirdError, UserMessage, FileMessage, AdminMessage } from 'sendbird';
import { StoreState } from 'src/handlers/store/store';
import { connect } from 'react-redux';
import { ErrorActions, addError } from 'src/handlers/store/errors/actions';
import { ChannelActions, setCurrentChannel, addChannel, initChannel } from 'src/handlers/store/channels/actions';
import { Dispatch } from 'redux';

import ChatStyle from './ChatStyle'
import MessagesHandler from './MessagesHandler'
import Loader from '../stateless/Loader/Loader'
import ChannelsHandler from './ChannelsHandler'


export interface Props {
  addSbError: (error: SendBirdError) => void
  addChannel: (channels: OpenChannel|GroupChannel, index: number|void) => void
  initChannel: (channel: Array<OpenChannel|GroupChannel>) => void
  setCurrentChannel: (channel: OpenChannel|GroupChannel) => void
  sb: SendBirdInstance
  currentChannel: OpenChannel | GroupChannel
  channels: Array<OpenChannel|GroupChannel>
  username: string
}

interface State {
  listeners: number
}

/* Chat Page devrait disparaître, pour le moment c'est un composant de gestions de channel pour l'initialisation ET 
un composant qui structure les fonctionnalités du chat */

export class ChatPage extends React.Component<Props, State> {
  constructor(props: Props){
    super(props)
    this.state = {
      listeners: 0,
    }
    const defaultOpenChannel = ['general']
    this.eventSubscribe = this.eventSubscribe.bind(this)
    defaultOpenChannel.forEach(channelName => this.joinOpenChannel(channelName))
    this.loadGroupChannels()
  }

  joinOpenChannel(channelName: string, shouldFocus: boolean = true){
    this.props.sb.OpenChannel.getChannel(channelName, (channel, error) => {
      if (error) this.props.addSbError(error)
  
      channel.enter((response, error) => {
        if (error) this.props.addSbError(error)
        this.props.addChannel(channel, 0)
        if(shouldFocus) this.props.setCurrentChannel(channel)
      });
    });
  }

  loadGroupChannels(){
    const filteredQuery = this.props.sb.GroupChannel.createMyGroupChannelListQuery();
    filteredQuery.userIdsFilter = [ this.props.username ];
    filteredQuery.includeEmpty = true
    filteredQuery.next((channels, error) => {
      if (error) this.props.addSbError(error)
      this.props.initChannel([...this.props.channels, ...channels])
    });
  }


  eventSubscribe = (listener: (channel: OpenChannel | GroupChannel, message: UserMessage | FileMessage | AdminMessage) => void) => {
    const ChannelHandler = new this.props.sb.ChannelHandler();
    ChannelHandler.onMessageReceived = listener
    this.setState({listeners: this.state.listeners +1})
    this.props.sb.addChannelHandler(''+this.state.listeners, ChannelHandler);
  }

  render(){
    const ready = this.props.currentChannel !== undefined
    return(
      <div id="chatPage" className='default'>
        <ChatStyle>
          <div id='chatFunctionnalities'>
            <ChannelsHandler />
            <div id="conversation">
              { ready ? 
                <MessagesHandler 
                  channel={ this.props.currentChannel as OpenChannel|GroupChannel } 
                  addSbError={ this.props.addSbError} 
                  eventSubscribe={this.eventSubscribe}
                /> :
                <Loader/>
              }
            </div>
          </div>
        </ChatStyle>
      </div>      
    )
  }
}

function mapStateToProps({ channels, sendbird, username }: StoreState) {
  return {
    sb: sendbird.sb,
    channels: channels.channels,
    currentChannel: channels.currentChannel,
    username: username
  }
}

function mapDispatchToProps(dispatch: Dispatch<ErrorActions | ChannelActions>) {
  return {
    addError: (error: string): void => { dispatch(addError(error)) },
    addSbError: (error: SendBird.SendBirdError): void => { dispatch(addError("Sendbird error " + error.code + ": " + error.message)) },
    setCurrentChannel: (channel: OpenChannel|GroupChannel): void => { dispatch(setCurrentChannel(channel)) },
    initChannel: (channel: Array<OpenChannel|GroupChannel>): void => { dispatch(initChannel(channel)) },
    addChannel: (channel: OpenChannel|GroupChannel, index: number|void): void => { dispatch(addChannel([channel], index)) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatPage);