import * as React from 'react'
import { OpenChannel, GroupChannel, SendBirdError, PreviousMessageListQuery, FileMessage, AdminMessage, UserMessage } from 'sendbird';

import ListMessage from '../stateless/ListMessage/ListMessage'
import SendMessage from './SendMessage'


export interface Props {
  channel: GroupChannel | OpenChannel
  eventSubscribe: (listener: (channel: OpenChannel | GroupChannel, message: UserMessage | FileMessage | AdminMessage) => void) => void
  addSbError: (error: SendBirdError) => void
}

interface State {
  messages: Array<UserMessage | FileMessage | AdminMessage>
}

/* Gère les datas messages du channel envoyé en argument. Retourne la liste des messages et un formulaire d'écriture de message */

export default class MessagesHandler extends React.Component<Props, State> {
  constructor(props: Props){
    super(props)
    this.state = {
      messages: []
    }
    this.addMsg = this.addMsg.bind(this)
    this.loadPrevious = this.loadPrevious.bind(this)
    this.props.eventSubscribe(this.addMsg)
    this.init(this.props.channel)
  }

  private listQuery: PreviousMessageListQuery

  componentWillReceiveProps(nextProps: Props){
    if(nextProps.channel !== this.props.channel){
      this.init(nextProps.channel)
    }
  }

  private init(channel: OpenChannel|GroupChannel){
    this.setState({ messages: [] })
    this.listQuery = channel.createPreviousMessageListQuery();
    this.loadPrevious()
  }

  private loadPrevious = () => {
    if(this.listQuery.isLoading) return
    this.listQuery.load(25, false, (messageList, error) => {
      if (error) { this.props.addSbError(error)}
      this.setState({
        messages: [...messageList, ...this.state.messages]
      })
    });
  }

  private addMsg = (channel: OpenChannel | GroupChannel, message: UserMessage | FileMessage | AdminMessage): void => {
    if(channel == this.props.channel){
      this.setState({
        messages: [...this.state.messages, message]
      })
    }
  }

  render(){
    return(
      <div id="messageHdlr" className="default">
        <ListMessage loadPrevious={this.loadPrevious} listMessages={this.state.messages} />
        <SendMessage 
          addMsg = { this.addMsg }
          channel={ this.props.channel as OpenChannel|GroupChannel } 
          addSbError={ this.props.addSbError} 
        />
      </div>
    )
  }
}