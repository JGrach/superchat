import * as React from 'react'

import FormCreate from '../stateless/FormCreate/FormCreate'

export interface Props {
  createChannel: (usersIds: string[], shouldFocus?:boolean) => void
  closeModal: (e:React.MouseEvent<HTMLButtonElement>) => void
}

interface State {
  userId: string
  userIds: string[]
}

/* Composant pour gérer les données inputs du formulaire de creation de channel */

export default class CreateChannel extends React.Component<Props, State> {
  constructor(props: Props){
    super(props)
    this.state = {
      userId: "",
      userIds: []
    }
  }

  private onChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    this.setState({ userId: e.target.value })
  }

  private onSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    this.setState({
      userId: "",
      userIds: [...this.state.userIds, this.state.userId]
    })
  }

  private createChannel = (e: React.MouseEvent<HTMLButtonElement>) => {
    this.props.createChannel(this.state.userIds)
    this.setState({
      userIds: []
    })
  }

  render(){
    const submitValue = "create".toUpperCase() // maybe will join a dictionnary to language 
    const cancelValue = "cancel".toUpperCase()
    return(
      <div id='createChannel'>
        <FormCreate userId={this.state.userId} onChange={ this.onChange } onSubmit = { this.onSubmit }/>
        <ul>{this.state.userIds.map( (userId, i) => <li key={i}>{userId}</li>) }</ul>
        <div>
          <button onClick={this.createChannel}>{submitValue}</button>
          <button onClick={this.props.closeModal}>{cancelValue}</button>
        </div>
      </div>
      
    )
  }
}