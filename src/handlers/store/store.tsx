import { createStore, combineReducers, Reducer } from 'redux';

import { username } from './username/reducer';
import { UsernameActions } from './username/actions'
import { errors } from './errors/reducer';
import { ErrorActions } from './errors/actions'
import { sendbird, sendbirdState } from './sendbird/reducer';
import { SendbirdActions } from './sendbird/actions'
import { channels, channelState } from './channels/reducer';
import { ChannelActions } from './channels/actions'

export interface StoreState {
  username: string
  errors: string[]
  sendbird: sendbirdState
  channels: channelState
}

type actions = UsernameActions | ErrorActions | SendbirdActions | ChannelActions

const reducers: Reducer<StoreState> = combineReducers<StoreState>({ username, errors, sendbird, channels })

export const store = createStore<StoreState, actions, void, void>(reducers);