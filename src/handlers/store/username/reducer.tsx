import { UsernameActions } from './actions';
import { SET_USERNAME, DEL_USERNAME } from './constants';

export function username(state: string = "", action: UsernameActions): string {
  switch (action.type) {
    case SET_USERNAME:
      return action.username;
    case DEL_USERNAME:
      return "";
  }
  return state;
}