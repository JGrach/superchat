import * as constants from './constants';

export interface SetUsername {
    type: constants.SET_USERNAME;
    username: string;
}

export interface DelUsername {
    type: constants.DEL_USERNAME;
}

export type UsernameActions = SetUsername | DelUsername;

export function setUsername(username: string): SetUsername {
    return {
        type: constants.SET_USERNAME,
        username
    }
}

export function delUsername(): DelUsername {
    return {
        type: constants.DEL_USERNAME
    }
}