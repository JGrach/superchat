import { ErrorActions } from './actions';
import { ADD_ERROR, DEL_ERRORS } from './constants';

export function errors(state: string[] = [], action: ErrorActions): string[] {
  switch (action.type) {
    case ADD_ERROR:
      return [...state, action.error];
    case DEL_ERRORS:
      return [];
  }
  return state;
}