import * as constants from './constants';

export interface AddError {
    error: string;
    type: constants.ADD_ERROR;
}

export interface DelErrors {
    type: constants.DEL_ERRORS;
}

export type ErrorActions = AddError | DelErrors;

export function addError(error: string): AddError {
    return {
        error,
        type: constants.ADD_ERROR
    }
}

export function delErrors(): DelErrors {
    return {
        type: constants.DEL_ERRORS
    }
}