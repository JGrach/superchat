export const ADD_ERROR = 'ERRORS/ADD_ERROR';
export type ADD_ERROR = typeof ADD_ERROR;

export const DEL_ERRORS = 'ERRORS/DEL_ERRORS';
export type DEL_ERRORS = typeof DEL_ERRORS;