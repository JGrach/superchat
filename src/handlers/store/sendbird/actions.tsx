import * as constants from './constants';
import { SendBirdInstance, User } from 'sendbird';

export interface SetSendbird {
    type: constants.SET_SENDBIRD;
    sb: SendBirdInstance;
    owner: SendBird.User;
}

export interface DelSendbird {
    type: constants.DEL_SENDBIRD;
}

export type SendbirdActions = SetSendbird | DelSendbird;

export function setSendbird(sb: SendBirdInstance, owner: User): SetSendbird {
    return {
        type: constants.SET_SENDBIRD,
        sb,
        owner
    }
}

export function delSendbird(): DelSendbird {
    return {
        type: constants.DEL_SENDBIRD
    }
}