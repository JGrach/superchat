import { SendbirdActions } from './actions';
import { SET_SENDBIRD, DEL_SENDBIRD } from './constants';
import { SendBirdInstance, User } from 'sendbird';

export interface sendbirdState {
  initialized: boolean;
  sb?: SendBirdInstance;
  owner?: User;
}

export function sendbird(state: sendbirdState = { initialized: false }, action: SendbirdActions): sendbirdState {
  switch (action.type) {
    case SET_SENDBIRD:
      const { sb, owner } = action
      return { initialized: true, sb, owner };
    case DEL_SENDBIRD:
    return { initialized: false };
  }
  return state;
}