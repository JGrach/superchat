import * as constants from './constants';
import { GroupChannel, OpenChannel } from 'sendbird';

export interface InitChannel {
    type: constants.INIT_CHANNEL;
    channels: Array<OpenChannel | GroupChannel>;
}

export interface AddChannel {
    type: constants.ADD_CHANNEL;
    channels: Array<OpenChannel | GroupChannel>;
    index: number|void
}

export interface RemoveChannel {
    type: constants.REMOVE_CHANNEL;
    channels: Array<OpenChannel | GroupChannel>;
}

export interface DelChannel {
    type: constants.DEL_CHANNEL;
}

export interface SetCurrentChannel {
    type: constants.SET_CURRENTCHANNEL;
    channel: OpenChannel | GroupChannel;
}

export type ChannelActions = InitChannel | 
    AddChannel | 
    RemoveChannel | 
    DelChannel | 
    SetCurrentChannel

export function initChannel(channels: Array<OpenChannel | GroupChannel>): InitChannel {
    return {
        channels,
        type: constants.INIT_CHANNEL
    }
}

export function addChannel(channels: Array<OpenChannel | GroupChannel>, index: number|void): AddChannel {
    return {
        channels,
        index,
        type: constants.ADD_CHANNEL
    }
}

export function removeChannel(channels: Array<OpenChannel | GroupChannel>): RemoveChannel {
    return {
        channels,
        type: constants.REMOVE_CHANNEL
    }
}

export function delChannel(): DelChannel {
    return {
        type: constants.DEL_CHANNEL
    }
}

export function setCurrentChannel(channel: GroupChannel | OpenChannel): SetCurrentChannel {
    return {
        channel,
        type: constants.SET_CURRENTCHANNEL
    }
}