import { ChannelActions } from './actions';
import { INIT_CHANNEL, ADD_CHANNEL, REMOVE_CHANNEL, DEL_CHANNEL, SET_CURRENTCHANNEL } from './constants';
import { GroupChannel, OpenChannel } from 'sendbird';

export interface channelState {
  channels: Array<OpenChannel | GroupChannel>;
  currentChannel: GroupChannel | OpenChannel | void
}

export function channels(state: channelState = { channels: [], currentChannel: undefined }, action: ChannelActions) {
    switch (action.type) {
      case INIT_CHANNEL:
        return { 
          channels: action.channels,  
          currentChannel: state.currentChannel
        };
      case ADD_CHANNEL:
        const channels = !action.index && action.index !== 0 ? [...state.channels, ...action.channels] :
          state.channels.slice(0, action.index ).concat(action.channels).concat(state.channels.slice(action.index))
        return { 
          channels,  
          currentChannel: state.currentChannel
        };
      case REMOVE_CHANNEL:
        return { 
          channels: state.channels.filter( chan => !action.channels.includes(chan) ),  
          currentChannel: state.currentChannel // TODO reference issue: verify immutability and references
        };
      case DEL_CHANNEL:
        return { 
          channels: [],  
          currentChannel: state.currentChannel
        };
      case SET_CURRENTCHANNEL:
        return { 
          channels: state.channels,  
          currentChannel: action.channel
        };
  }
  return state;
}