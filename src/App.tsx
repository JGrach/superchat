import * as React from 'react';
import Router from './stateful/Router'

class App extends React.Component {
  public render() {
    return (
      <Router/>
    );
  }
}

export default App;
