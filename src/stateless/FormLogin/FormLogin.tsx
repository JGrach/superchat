import * as React from 'react';

import './FormLogin.css'

export interface Iprops {
  username: string;
  onSubmit(e: React.FormEvent<HTMLFormElement>): void;
  onChange(e: React.ChangeEvent<HTMLInputElement>): void;
}

export default function FormLogin(props: Iprops) {
  const submitValue = "Let me in".toUpperCase() // maybe will join a dictionnary to language 
  return (
    <form id="formLogin" onSubmit={ props.onSubmit }>
      <input type="text" name="username" id="username" placeholder="Username" onChange={ props.onChange } value={ props.username }/>
      <input type="submit" value={submitValue}/>
    </form>
  )
}