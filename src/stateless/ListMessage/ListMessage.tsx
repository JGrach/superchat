import * as React from 'react';
import { UserMessage, FileMessage, AdminMessage } from 'sendbird';

import './ListMessage.css'

export interface Props {
  loadPrevious():void
  listMessages: Array<UserMessage | FileMessage | AdminMessage>
}

export default function ListMessage (props: Props){
  const refScroller = (el: HTMLDivElement):void => {
    el.addEventListener('scroll', (e => {
      e.preventDefault()
      if(el.scrollTop == 0) {
        props.loadPrevious()
      }
    }))
  }

  const displayMessage = ( message: UserMessage, i:number ) => 
    (<li key={i} className='message'> 
      <img src={message.sender.profileUrl} alt='usr profil' /> 
      <span className="username">{ message.sender.userId }</span>  {/* what if name too long ?*/}
      {message.message} 
    </li>)

  return (
    <div id="listMessage" ref={(el) => { if(el) refScroller(el as HTMLDivElement) }}>
      <ul>
        { props.listMessages.map((msg, i) => displayMessage(msg as UserMessage, i)) }
        <li ref={(el) => { if(el) el.scrollIntoView() } }/> {/* issue: load previous scroll to bottom */}
      </ul>
    </div>
  )
}