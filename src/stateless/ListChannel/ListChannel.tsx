import * as React from 'react';
import { GroupChannel, OpenChannel } from 'sendbird';

import './ListChannel.css'

export interface Props {
  currentChannel: GroupChannel | OpenChannel
  listChannels: Array<GroupChannel | OpenChannel>
  changeChannel: (channel: OpenChannel|GroupChannel) => void
  removeChannel: (channel: OpenChannel|GroupChannel) => void
}

export default function ListChannels(props: Props){

  const displayMessage = ( chan: OpenChannel|GroupChannel, i:number ) => {
    const changeChannel = (e: React.MouseEvent<HTMLLIElement>) => {
      props.changeChannel(chan)
    }
    const removeChannel = (e: React.MouseEvent<HTMLImageElement>) => {
      props.removeChannel(chan)
    }

    return (
      <li 
        key={i} 
        className={chan == props.currentChannel ? 'select' : ''}
        onClick={ changeChannel }> 
        <img src="./close.png" alt="close" onClick={removeChannel}/> { chan.name } 
      </li>
      )
  }
  
  return (
    <div id="listChannel">
      <ul>
        { props.listChannels.map((chan, i) => displayMessage(chan, i)) }
      </ul>
    </div>
  )
}