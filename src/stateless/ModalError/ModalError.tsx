import * as React from 'react';

import './ModalError.css'

export default function ModalError(props: { errors: string[] }){
  return (
    <div id="modalError">
      <h2>Errors</h2>
      { 
        props.errors.map( (error, i) => <li key={i}>{error}</li>)
      }
    </div>
  )
}