import * as React from 'react';

import LoggingIn from '../../stateful/LoggingIn'
import './Login.css'

export default function Login(){
  return (
    <div id="login">
      <h1>SuperChat</h1>
      <LoggingIn/>
    </div>
  )
}