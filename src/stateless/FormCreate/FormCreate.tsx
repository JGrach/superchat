import * as React from 'react';

import './FormCreate.css'

export interface Props {
  userId: string;
  onSubmit(e: React.FormEvent<HTMLFormElement>): void;
  onChange(e: React.ChangeEvent<HTMLInputElement>): void;
}

export default function FormCreate(props: Props) {
  const submitValue = "Add".toUpperCase() // maybe will join a dictionnary to language 
  return (
    <form id="formCreate" onSubmit={ props.onSubmit }>
      <input type="text" name="channelName" id="channelName" placeholder="type a userId" onChange={ props.onChange } value={ props.userId }/>
      <input type="submit" value={submitValue}/>
    </form>
  )
}