import * as React from 'react';

import './FormMessage.css'

export interface Iprops {
  message: string;
  onSubmit(e: React.FormEvent<HTMLFormElement>): void;
  onChange(e: React.ChangeEvent<HTMLInputElement>): void;
}

export default function FormMessage(props: Iprops) {
  const submitValue = "Send".toUpperCase() // maybe will join a dictionnary to language 
  return (
    <form id="formMessage" onSubmit={ props.onSubmit }>
      <input type="text" name="message" id="message" placeholder="Type your message" onChange={ props.onChange } value={ props.message }/>
      <input type="submit" value={submitValue}/>
    </form>
  )
}