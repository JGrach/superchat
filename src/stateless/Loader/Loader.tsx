import * as React from 'react';
import './Loader.css'

export default function Login(){
  return (
    <div id="loader" className="default">
      <img src="spinLoader.gif" alt="spin loader"/>
    </div>
  )
}