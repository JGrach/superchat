import * as React from 'react';
import './Header.css'

export default function Header(){
  return (
    <div id="header">
      <h1>SuperChat</h1>
    </div>
  )
}