# TECHNICAL FRONT TEST FOR WEMAINTAIN
## Summary
- Dependencies
- Install
- Justifications Techniques
- Architecture
- Step by step

## DEPENDENCIES
- Docker (test on 18.06.1-ce)
- Docker-Compose (test on 1.21.2)

## INSTALL

    git clone git@gitlab.com:JGrach/superchat.git && cd superchat && docker-compose build && docker-compose up -d

## JUSTIFICATIONS TECHNIQUES

### Librairies
- react : petite app mais très dynamique -> changement de page, création, suppressions onglets, conversations changes suivant onglets, ajout message ...
- sendbird (imposé)
- typescript: première fois que j'utilise typescript avec react mais typescript fait partie de la stack wemaintain et à l'avantage de la maintenabilité.

### Sys
- Dans la mesure où create-react-app suffit ici, on l'utilise

### Les tests
- J'ai pensé utiliser mocha et chai que je connais déjà couplé à enzyme et sinon pour tester les composants reacts. L'idée aurait été de faire des tests d'abords mais je connais mal enzyme et sinon et je ne suis pas sûr de comment tester mon front. Je préfère, pour des impératifs de temps et de cohérence m'abstenir d'implémenter moi même une architecture de tests. 

Concernant ce point, donc, je reste à disposition pour, par exemple, compléter en live une série de tests incomplète en m'appuyant sur une architecture déjà existante.

## STEP BY STEP
### ANALYSE DES BESOINS
L'app est un chat qui permet:
  1. Discussion dans un ou des open channel (ici on se limitera à un open channel général)
  2. Discussion dans des group channels entre utilisateurs prédéfinis
  3. Création de groupe channel en précisant les ids des users invité (joindre automatiquement)
  4. Onglets permettant de changer le channel courrant qui a le focus
  5. Suppression d'un onglet et donc quitte le channel
  6. Affichage des messages reçus par le channel courrant
  7. Affichage des messages de l'utilisateur si pas d'erreur dans réponse de SB
  8. Affichage des messages précédents au scroll top
  9. Affichage des utilisateurs du channel

### STRUCTURE DE L'APP

Après être mal parti, je suis revenu sur les fondamentaux. Puisque je n'arrivais pas à réfléchir correctement à l'architecture de l'app, je suis parti d'en bas. J'ai crée mes composants d'affichages et grâce au typage des props de typescript, j'ai pu faire la liste des datas dont j'avais vraiment besoin.
J'ai décidé de mettre la liste des channels accessibles par l'user dans le store parce que j'ai distigué trois pans de l'applications et qu'ils étaient tous directement lié aux informations des channels: La gestion des channels, la gestion des messages d'un channel et, ce qui n'existe pas encore (le composant est là mais pas implémenté), la gestion des membres d'un channel.

On trouve trois dossiers dans sources: 

  - Handlers qui ne contient finalement plus que les informations relative au store mais qui a vocation a être un support à toute l'app pour les fonctionnalités disponible partout.

  - Stateful: C'est la liste des composant à état.
    On y trouve le router à l'entrée qui envoie vers le système de login ou de chat
    Concernant le chat on a SendBirdMiddlware, ChatPage et ChannelHandler qui forme le haut du système et la gestion des channels. Il faudrait prendre le temps de les refactoriser car certaines de leur fonctionnalités font doublons ou sont mal défini.
    Les autres composants d'importances sont MessagesHandlers et ListMembers(pas encore implémenté) qui travaillent également la donnée sendbird.
    Puis les composants SendMessage et CreateChannel qui pourraient être factorisé et ne font que de la gestion de formulaire.

  - Stateless: Les composants sans état, les renders
    Là encore on peut noter que les trois composants de formulaires sont certainement factorisable.
    Login gère la page de Login, ModalError, envoyé depuis le router est une modal d'affichage d'erreurs, Loader est le spinner qui s'affiche quand les données ne sont pas prêtes ...

### LE STORE

  - Channels qui est la liste des channels disponibles par l'utilisateur
  - Errors qui est une liste d'erreurs sour formes de string à faire afficher par modal
  - Sendbird dont je ne suis absolument pas certain qui est l'instance sendbird actuelle
  - username qui correspond au login de l'utilisateur et qui fait complètement doublon pour le moment avec la partie owner de Sendbird


### PROBLEMES RENCONTRES
Intégration de SendBird.
Je ne suis pas tout à fait sûr de bien savoir pourquoi je me suis planté.

J'ai d'abord voulu intégré SendBird dans un système similaire à celui des API,
je voulais juste crée une instance d'api sendbird et fournir les méthodes nécessaires à son execution l'objet sendbird est très dynamique très complet et a déjà ce role en soit.

Je crois que ce qui m'a fait défaut c'est que je n'ai tout simplement pas su trouver le moyen malin de l'implémenter, en témoigne le composant SendBirdMiddleware, jadis Provider devenu en chemin une sorte de middleware de reducer pour gérer l'appel asynchrone avant la mise de SendBird en store.
Je ne suis pas plus satisfait de ça que du composant ChatPage qui est un mauvais mixte entre les composant SendBirdMiddlware qui fait l'initialisation et le composant ChannelsHandler qui gère les listes de channels.


Typescript
Sur beaucoup d'aspect, la prise en main de typescript, en plus d'être assez facile, a été d'une grande aide. En revanche typescript m'a pris un peu de temps notamment parce que je n'ai pas encore bien compris les différences entre types et interface et que je n'ai compris le typage dynamique (entre < >) qu'après un certain temps.

Pour être très honnête, je me suis aussi mis beaucoup de pression, votre offre est la plus intéressante des opportunités qu'on m'ait proposé pour le moment, et seul face à l'écran j'ai eu parfois un peu trop envie de bien faire sans savoir comment le faire.
