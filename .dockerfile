FROM node:9.11.2
EXPOSE 3000
COPY ./ /app/
RUN cd /app && \
    npm install
WORKDIR /app

ENTRYPOINT ["npm", "run"]
CMD [ "start" ]
